from pathlib import Path

from flask import request, render_template, send_from_directory, redirect, session, jsonify
from .app import app
from .models import User
from werkzeug.utils import secure_filename


@app.route("/users")
def test():
    users = User.query.all()

    return render_template("users.html", title="Users", users=users)


@app.route("/")
def start():
    return redirect("/home")


@app.route("/home")
def home():
    admin = session.get("_admin", False)
    return render_template("base.html", title="Home", admin=admin)


@app.route("/media/<path:filename>")
def mediafiles(filename):
    return send_from_directory(app.config["MEDIA_FOLDER"], filename)


@app.route("/static/<path:filename>")
def staticfiles(filename):
    return send_from_directory(app.config["STATIC_FOLDER"], filename)


@app.route("/upload", methods=['GET', 'POST'])
def render_upload():
    if request.method == "POST":
        try:
            file = request.files["file"]
            filename = secure_filename(file.filename)
            file.save(Path(app.config["MEDIA_FOLDER"], filename))
            if session.get("uploaded"):
                session["uploaded"].append(filename)
            else:
                session["uploaded"] = [filename]
            return render_template("success.html", title="Success")
        except Exception as e:
            return render_template("fail.html", title="Attempt failed", reason=e)

    return render_template('upload.html', title="Upload")


@app.route('/check')
def check():
    return jsonify({k: v for k, v in session.items()})


@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == "POST":
        username = request.form['username']
        user = User.query.filter_by(username=username).first()
        admin = session.get("_admin", False)
        return render_template("login.html", title="Login", username=username, admin=admin)

    return render_template("login.html", title="Login")
