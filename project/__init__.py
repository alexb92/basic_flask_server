from .app import app, db
from . import views
from .models import create_tables, User, TUser
from sqlalchemy import event
from sqlalchemy.engine import Engine
from sqlite3 import Connection as SQLite3Connection


@event.listens_for(Engine, "connect")
def _set_sqlite_pragma(dbapi_connection, connection_record):
    """
    This function acts as an event listener for Database Connection events. Once a connection of type "SQLite3" is
    detected, the SQL command to respect foreign key constraints is executed. This is ONLY necessary for SQLite3, most
    other database types automatically enforce foreign key restraints.
    """
    if isinstance(dbapi_connection, SQLite3Connection):
        cursor = dbapi_connection.cursor()
        cursor.execute("PRAGMA foreign_keys=ON;")
        cursor.close()

def create_db():
    db.drop_all()
    create_tables()

def seed_db():
    user1 = User(email="your.email@stud.fh-campuswien.ac.at", username="your_name", admin=True)
    user1.set_password("admin")
    tuser1 = TUser(t_id=1, test="Test")
    # tuser2 = TUser(id=2,t_id=2, test="Test")

    # Add user first, since there is a foreign key constraint in place
    db.session.add(user1)
    db.session.commit()
    # Then add tuser since it references the user ID
    db.session.add(tuser1)
    db.session.commit()

create_db()
seed_db()