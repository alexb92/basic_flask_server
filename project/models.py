from werkzeug.security import generate_password_hash, check_password_hash
from .app import db

def create_tables():
    meta = db.MetaData()

    User_Creation = db.Table(
        'users',
        meta,
        db.Column("id", db.Integer, primary_key=True, nullable=False),
        db.Column("email", db.String(80), unique=True),
        db.Column("username", db.String(100)),
        db.Column("password_hash", db.String()),
        db.Column("admin", db.Boolean())
    )

    TUser_Creation = db.Table(
        'tusers',
        meta,
        db.Column("id", db.Integer, primary_key=True),
        db.Column("t_id", db.Integer, nullable=False),
        db.Column("test", db.String(50)),
        db.ForeignKeyConstraint(['t_id'], ['users.id'])
    )

    meta.create_all(db.engine)


class User(db.Model):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(80), unique=True)
    username = db.Column(db.String(100))
    password_hash = db.Column(db.String())
    admin = db.Column(db.Boolean())

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def get_public_info(self):
        return self.username, self.email

    def __repr__(self):
        return f"User: {self.username}; Email: {self.email}"


class TUser(db.Model):
    __tablename__ = 'tusers'

    id = db.Column(db.Integer, primary_key=True)
    t_id = db.Column(db.Integer, nullable=False)
    test = db.Column(db.String(100))
    db.ForeignKeyConstraint(['t_id'], ['users.id'])

    def __repr__(self):
        return f"User ID: {self.id}, Username: {self.t_id}"


