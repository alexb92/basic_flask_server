import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy

# App
app = Flask(__name__)
app.config.from_object("project.config.Config")
app.secret_key = "RandomSecretKey"

# Database
db = SQLAlchemy(app)