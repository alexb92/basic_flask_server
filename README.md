![Flask Logo](https://flask.palletsprojects.com/en/1.1.x/_static/flask-icon.png "Flask")
# **Basic Flask Server**
***This project serves as a showcase and template to expand upon for a flask web server***
******

***A detailed tutorial can be found [here](https://flask.palletsprojects.com/en/1.1.x/tutorial/).***
***

### Summary
This flask web server will host a few very basic sites to showcase the concept.

Currently, the following URLs are available:
/home
/upload
/users
/check

***
### Configuration
Simply create a virtual environment using the provided pip-file.

Should you not be familiar with pipenv, you can also use the requirements.txt file to install all packages.

### Running the server
1) Open a terminal or use the terminal provided by your IDE
2) Navigate to the "project" folder
3) Use command "flask run" to start server
4) Profit